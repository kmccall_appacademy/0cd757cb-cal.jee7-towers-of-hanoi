# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :towers, :discs, :disc_array

  def initialize(discs=3)
    @discs = discs
    @disc_array = (1..discs).to_a
    @towers = [disc_array.reverse, [], []]
  end

  def play
    until won?
      render
      from_tower = nil
      to_tower = nil
      until (0..@towers.length).to_a.include?(from_tower)
        puts "Please select a tower (0, 1, or 2) FROM which you would like to move the top disc?"
        from_tower = ask_tower
      end
      until (0..@towers.length).to_a.include?(to_tower)
        puts "Please select a tower (0, 1, or 2) TO which you would like to place this disc?"
        to_tower = ask_tower
      end

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
      else
        puts "*That is not a valid move, try again.*"
        from_tower = nil
        to_tower = nil
      end
    end
    render
    puts "****CONTRALUATIONS! WINNER WINNER CHICKEN DINNER!****"
  end

  def ask_tower
    begin
      tower = Integer(gets)
    rescue ArgumentError
      puts "*That is not a valid tower.*"
      tower = nil
    end
    tower
  end

  def move(from_tower, to_tower)
    disc = @towers[from_tower].pop
    @towers[to_tower] << disc
  end

  def valid_move?(from_tower, to_tower)
    moving_disc = @towers[from_tower].last
    compare_disc = @towers[to_tower].last
    if moving_disc && moving_disc <= (compare_disc || @discs)
      return true
    end
    false
  end

  def won?
    @towers[1] == disc_array.reverse || @towers[2] == disc_array.reverse
  end

  def render
    puts "\n"
    spaces = discs * 2 + 1
    disc_pos = discs - 1
    discs.times do |i|
      @towers.each do |tower|
        if !tower[disc_pos]
          print "|" + " "*spaces + "|"
        else
          disc_lines = tower[disc_pos] * 2 - 1
          num_disc_lines = "-" * disc_lines
          num_spaces = " " * ((spaces - disc_lines) / 2)
          print "|" + num_spaces + num_disc_lines + num_spaces + "|"
        end
      end
      disc_pos -= 1
      puts ""
    end
    puts "\n"
  end
end

game = TowersOfHanoi.new(3)
game.play
